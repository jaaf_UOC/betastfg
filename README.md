# Trabajo de fin de grado ( repositorio de trabajo ) 
![img title](titulo.jpg)

<pre>Repositorio realizado para la implementación del proyecto referente al 
trabajo de fin de grado de  ingeniería informática en la Universitat Oberta
de Catalunya dentro del área de ingeniería del software. El tiempo utilizado
para todo el TFG es de un cuatrimestre y se parte sin conocimientos del entorno
y ningún otro dentro de este ámbito especifico.

El Trabajo de Final de Grado se basa en realizare un videojuego utilizando
y desarrollando las aptitudes adquiridas durante el grado de ingeniería
informática en la Universitat Oberta de Catalunya. El desarrollo se sustenta
en el motor gráfico Unreal Engine 5, motor de carácter general muy extendido
dentro de los videojuegos, y la utilización de assets sólo para el apartado
gráfico desarrollándose para el TFG todo el diseño y la programación en C++.

En el transcurso del trabajo se busca obtener un producto entregable y jugable
dentro del segmento del PC, basado en el género de las aventuras de acción
dónde se implementan las mecánicas y avances propias del género. Dicho genero
trata de explicar una historia a medida que el jugador avanza en busca de
objetivos que le permitan finalizar la obra, para ello, dentro de esta historia
se introducen distintos tipos de fases que ayuden a crear un entorno dinámico
que atraiga y cautive al participante.

Se pretende obtener un producto de calidad acorde con los estándares actuales
de la industria del sector del videojuego, aunque con una duración mucho menor
pero suficiente para transmitir una experiencia agradable.</pre>

Este repositorio se encuentra enlazado en el repositorio institucional O2
de la Universitat Oberta de Catalunya (UOC), en donde se puede consultar 
la memoria del proyecto.

[Enlace a la memoria del TFG en O2 de la UOC](http://hdl.handle.net/10609/148231) 

## Componentes

1. ProjectECO Trailer FHD.mp4 <br>
   Muestra un trailer del juego de tres minutos de duración. <br>

2. ProjectEcoUltimateEdition.zip <br>
   Juego completo del proyecto en formato comprimido zip. <br>

## Instalación

 1. Clona o descarga los ficheros.
 2. ProjectECO Trailer FHD.mp4 visualizar en cualquier reproductor multimedia compatible.
 3. ProjectEcoUltimateEdition.zip descomprimir la carpeta en un directorio. <br>
    3.1. Entrar en la carpeta Windows <br>
    3.2. Ejecutar ProjectTFG_U_v1.exe.

## Requisitos del sistema
El proyecto está realizado para trabajar en un sistema basado en las siguientes especificaciones técnicas. <br>

* Sistema Operativo: Windows 10 o superior 
* CPU: AMD o Intel gama media 
* Memoria RAM: 8GB 
* DirectX: versión 11 o 12 (aconsejada) 
* Espacio en disco: 4GB
* Teclado y ratón o gamepad 